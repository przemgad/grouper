using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using FluentAssertions;
using Moq;
using Projekt.Core.Domain;
using Projekt.Core.Repositories;
using Projekt.Infrastructure.DTO;
using Projekt.Infrastructure.Services;
using Xunit;

namespace Projekt.Tests.Service {
    public class UserServiceTests {

        private readonly Mock<IUserRepository> _userRepositoryMoq = new Mock<IUserRepository> ();
        private readonly Mock<IMapper> _mapperMoq = new Mock<IMapper> ();
        [Fact]
        public async Task userService_should_invoke_AddAsync_on_userRepostiory () {
                var testUser = new User (Guid.NewGuid (), "tester", "emial@mail.com", "hash");
                var _userService = new UserService (_userRepositoryMoq.Object, _mapperMoq.Object);

                await _userService.CreateAsync (testUser.Id, testUser.Name, testUser.Email, testUser.Password, testUser.Role);

                _userRepositoryMoq.Verify (x => x.AddAsync (It.IsAny<User> ()), Times.Once);

            }
            [Fact]
        public async Task GetAsync_should_invoking_userRepository_once_and_return_UserDTO () {
            var testUser = new User (Guid.NewGuid (), "tester", "emial@mail.com", "hash");
            var _userService = new UserService (_userRepositoryMoq.Object, _mapperMoq.Object);
            _mapperMoq.Setup(x => x.Map<UserDTO>(testUser)).Returns(new UserDTO{
                Id = testUser.Id,
                Role = testUser.Role,
                Name = testUser.Name,
                Email = testUser.Email,
                Latitude = testUser.Latitude,
                Longitude = testUser.Longitude,
            });

            _userRepositoryMoq.Setup (x => x.GetAsync (testUser.Id)).ReturnsAsync (testUser);

            var userDTO = await _userService.GetAsync (testUser.Id);
            _userRepositoryMoq.Verify (x => x.GetAsync (testUser.Id), Times.Once);
            userDTO.Should ().NotBeNull ();
            userDTO.Email.Should().BeEquivalentTo(testUser.Email);
            userDTO.Id.Should().Equals(testUser.Id);
        }
    }

}