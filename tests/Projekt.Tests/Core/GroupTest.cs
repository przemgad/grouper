using System;
using FluentAssertions;
using Projekt.Core.Domain;
using Xunit;

namespace Projekt.Tests.Core
{
    public class GroupTest
    {
        [Fact]
        public void setName_should_change_Name_when_Name_is_Valid()
        {
        string name = "MyGroup2";
        var group = Group.CreateGroup(Guid.NewGuid(), "MyGroup", "My first group for test" );
        
        //When
        group.SetName(name);
        //Then
        group.Name.Should().Equals(name);
        }
        [Fact]
        public void setName_should_throw_exception_when_Name_is_inValid()
        {
        string name = "My";
        var group = Group.CreateGroup(Guid.NewGuid(), "MyGroup", "My first group for test" );
        
        //When
        Assert.Throws<Exception>(()=>group.SetName(name));
        }
        [Fact]
        public void setName_should_throw_exception_when_Name_is_null()
        {
        string name = null;
        var group = Group.CreateGroup(Guid.NewGuid(), "MyGroup", "My first group for test" );
        
        //When
        Assert.Throws<Exception>(()=>group.SetName(name));
        }
    }
}