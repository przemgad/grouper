using System;
using FluentAssertions;
using Projekt.Core.Domain;
using Xunit;

namespace Projekt.Tests.Core {
    public class UserTest {
        [Fact]
        public void id_should_be_type_Guid () {
            //Given
            User user = new User (Guid.NewGuid (), "testowy", "emial@mail.com", "hash");

            //When

            //Then
            Assert.IsType<Guid> (user.Id);
        }

        [Fact]
        public void setName_should_change_username_if_name_is_valid () {
                //Given
                string name = "new Name";
                User user = new User (Guid.NewGuid (), "testowy", "emial@mail.com", "hash");
                //When
                user.setName (name);
                //Then
                user.Name.Should ().BeEquivalentTo (name);
            }
            [Fact]
        public void setName_should_throw_exception_if_name_is_Null () {
                //Given
                User user = new User (Guid.NewGuid (), "testowy", "emial@mail.com", "hash");
                //When

                //Then
                Assert.Throws<Exception> (() => user.setName (null));

            }
            [Fact]
        public void setName_should_throw_exception_if_name_is_invalid () {
                //Given
                string name = "new";
                User user = new User (Guid.NewGuid (), "testowy", "emial@mail.com", "hash");
                //When

                //Then
                Assert.Throws<Exception> (() => user.setName (name));

            }
            [Fact]
        public void setEmail_should_change_email_if_email_is_valid () {
                //Given
                string email = "emaill@gmail.com";
                User user = new User (Guid.NewGuid (), "testowy", "emial@mail.com", "hash");
                //When
                user.setEmail (email);
                //Then
                user.Email.Should ().BeEquivalentTo (email);

            }
            [Fact]
        public void setEmail_should_throw_exception_if_email_is_null () {
                //Given
                string email = null;
                User user = new User (Guid.NewGuid (), "testowy", "emial@mail.com", "hash");
                //When
                //Then
                Assert.Throws<Exception> (() => user.setEmail (email));

            }
            [Fact]
        public void setEmail_should_throw_exception_if_email_is_invalid () {
            //Given
            string email = "@mail.com";
            User user = new User (Guid.NewGuid (), "testowy", "emial@mail.com", "hash");
            //When
            //Then
            Assert.Throws<Exception> (() => user.setEmail (email));

        }

    }
}