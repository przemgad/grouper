namespace Projekt.Infrastructure.Commands
{
    public class UpdateLocation
    {
        public double Latitude { get;  set; }
        public double Longitude { get;  set; }
    }
}