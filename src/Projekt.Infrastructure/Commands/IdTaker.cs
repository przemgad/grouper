using System;

namespace Projekt.Infrastructure.Commands
{
    public class IdTaker
    {
        public Guid Id { get; set; }
    }
}