using System;
using Projekt.Core.Domain;

namespace Projekt.Infrastructure.Commands {
    public class CreateGroup {
        public Guid Id { get; set; }
        public User Head { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}