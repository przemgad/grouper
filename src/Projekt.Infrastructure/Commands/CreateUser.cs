using System;
using Projekt.Core.Domain;

namespace Projekt.Infrastructure.Commands {
    public class CreateUser {
        public Guid Id { get; set; }
        public string Role { get; set;}
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public double? Latitude { get; protected set; }
        public double? Longitude { get; protected set; }

    }
}