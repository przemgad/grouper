namespace Projekt.Infrastructure.Command {
    public class EditGroup {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}