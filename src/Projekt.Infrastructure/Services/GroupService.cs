using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Projekt.Core.Domain;
using Projekt.Core.Repositories;
using Projekt.Infrastructure.DTO;
using Projekt.Infrastructure.Repository.ExtensionMethod;

namespace Projekt.Infrastructure.Services {
    public class GroupService : IGroupService {
        private readonly IGroupRepository _groupRepostiory;
        private readonly IUserRepository _userRepostiory;
        private readonly IMapper _mapper;
        public GroupService (IGroupRepository groupRepository, IUserRepository userRepository, IMapper mapper) {
            _groupRepostiory = groupRepository;
            _userRepostiory = userRepository;
            _mapper = mapper;

        }

        public async Task<GroupDTO> GetAsync (Guid id) {
            var group = await _groupRepostiory.GetOrFailAsync (id);
            return _mapper.Map<GroupDTO> (group);
        }

        public async Task<GroupDTO> GetAsync (string name) {
            var group = await _groupRepostiory.GetOrFailAsync (name);
            return _mapper.Map<GroupDTO> (group);
        }
        public async Task<IEnumerable<GroupDTO>> BrowseAsync (string name = null) {
            var groups = await _groupRepostiory.BrowseAsync (name);

            return _mapper.Map<IEnumerable<GroupDTO>> (groups);
        }

        public async Task CreateAsync (Guid id, string name, string description) {
            var group = await _groupRepostiory.GetAsync (name);
            if (group != null)
                throw new Exception ($"Group with name:{group.Name} already exist");
            await _groupRepostiory.AddAsync (Group.CreateGroup (id, name, description));
        }

        public async Task DeleteAsync (Guid id) {
            var group = await _groupRepostiory.GetAsync (id);
            if (group != null)
                await _groupRepostiory.RemoveAsync (group);
            else
                throw new Exception ($"Group with id:{id} does not exist");
        }

        public async Task UpdateAsync (Guid id, string name, string description) {
            var group = await _groupRepostiory.GetAsync (name);
            if (group != null) {
                throw new Exception ($"Group with name:{name} already exist.");
            }
            group = await _groupRepostiory.GetOrFailAsync (id);
            group.SetName (name);
            group.SetDescription (description);

        }

        public async Task AddMemberAsync(Guid groupId, Guid userId)
        {
            var group = await _groupRepostiory.GetOrFailAsync(groupId);
            var user = await _userRepostiory.GetOrFailAsync(userId);
            group.AddToMembers(user);

        }

        public async Task RemoveMemberAsync(Guid groupId, Guid userId)
        {
            var group = await _groupRepostiory.GetOrFailAsync(groupId);
            var user = await _userRepostiory.GetOrFailAsync(userId);
            group.RemoveFromMembers(user);
        }
    }
}