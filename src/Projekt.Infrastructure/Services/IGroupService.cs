using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projekt.Core.Domain;
using Projekt.Infrastructure.DTO;

namespace Projekt.Infrastructure.Services {
    public interface IGroupService {
        Task<GroupDTO> GetAsync (Guid id);
        Task<GroupDTO> GetAsync (string name);
        Task<IEnumerable<GroupDTO>> BrowseAsync (string name = null);
        Task CreateAsync (Guid id, string name, string description);
        Task UpdateAsync (Guid id, string name, string description);
        Task DeleteAsync (Guid id);
        Task AddMemberAsync (Guid id, Guid user);
        Task RemoveMemberAsync (Guid groupId, Guid userId);

    }
}