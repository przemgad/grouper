using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Projekt.Core.Domain;
using Projekt.Core.Repositories;
using Projekt.Infrastructure.DTO;
using Projekt.Infrastructure.Repository.ExtensionMethod;

namespace Projekt.Infrastructure.Services {
    public class UserService : IUserService {
        private readonly IUserRepository _userRepostiory;
        private readonly IMapper _mapper;
        public UserService (IUserRepository userRepostiory, IMapper mapper) {
            _userRepostiory = userRepostiory;
            _mapper = mapper;

        }
        public async Task<IEnumerable<UserDTO>> BrowseAsync (string name = null) {
            var users = await _userRepostiory.BrowseAsync (name);
            return _mapper.Map<IEnumerable<UserDTO>> (users);
        }

        public async Task CreateAsync (Guid id, string name, string email, string password, string role = "user") {
            var user = await _userRepostiory.GetAsync (name);
            if (user != null)
                throw new Exception ($"User with name:{name} already exist.");
            else
                await _userRepostiory.AddAsync (new User (id, name, email, password));
        }

        public async Task DeleteAsync (Guid id) {
            var user = await _userRepostiory.GetOrFailAsync (id);
            await _userRepostiory.RemoveAsync (user);
        }

        public async Task<UserDTO> GetAsync (Guid id) {
            var user = await _userRepostiory.GetOrFailAsync (id);
            return _mapper.Map<UserDTO> (user);
        }

        public async Task<UserDTO> GetAsync (string name) {
            var user = await _userRepostiory.GetOrFailAsync (name);
            return _mapper.Map<UserDTO> (user);
        }

        public async Task UpdateAsync (Guid id, string name, string email, string password, string role = "user") {
            var user = await _userRepostiory.GetAsync (name);
            if (user != null) {
                throw new Exception ($"User with name:{name} already exist.");
            }
            user = await _userRepostiory.GetOrFailAsync (id);
            user.setName (name);
            user.setEmail (email);
        }

        public async Task<LocationDTO> GetLocationAsync (Guid id) {
            var user = await _userRepostiory.GetOrFailAsync (id);
            return _mapper.Map<LocationDTO> (user);
        }
        public async Task UpdateLocationAsync (Guid id, double latitude, double longitude) {
            var user = await _userRepostiory.GetOrFailAsync (id);
            user.setLocation (latitude, longitude);
        }
    }
}