using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projekt.Infrastructure.DTO;

namespace Projekt.Infrastructure.Services {
    public interface IUserService {
        Task<UserDTO> GetAsync (Guid id);
        Task<UserDTO> GetAsync (string name);
        Task<IEnumerable<UserDTO>> BrowseAsync (string name = null);
        Task CreateAsync (Guid id, string name, string email, string password, string role = "user");
        Task UpdateAsync (Guid id, string name, string email, string password, string role = "user");
        Task DeleteAsync (Guid id);
        Task<LocationDTO> GetLocationAsync (Guid id);
        Task UpdateLocationAsync (Guid id, double latitude, double longitude);
    }
}