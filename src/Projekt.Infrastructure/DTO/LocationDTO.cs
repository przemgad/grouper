using System;
using System.Collections.Generic;
using Projekt.Core.Domain;

namespace Projekt.Infrastructure.DTO {
    public class LocationDTO {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

    }
}