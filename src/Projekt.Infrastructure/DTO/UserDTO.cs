using System;

namespace Projekt.Infrastructure.DTO {
    public class UserDTO {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}