using System;
using System.Collections.Generic;
using Projekt.Core.Domain;

namespace Projekt.Infrastructure.DTO {
    public class GroupDTO {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<User> Members { get; set; }
    }
}