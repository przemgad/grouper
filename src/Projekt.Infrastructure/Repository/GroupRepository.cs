using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Core.Domain;
using Projekt.Core.Repositories;

namespace Projekt.Infrastructure.Repository {
    public class GroupRepository : IGroupRepository {
    private static readonly ISet<Group> _group = new HashSet<Group> () {
    Group.CreateGroup (Guid.NewGuid (), "Moja grupa", "nasza pierwsza grupa"),
    Group.CreateGroup (Guid.NewGuid (), "Moja 2", "nasza druga grupa"),
    Group.CreateGroup (Guid.NewGuid (), "PPPPP", "nasza druga grupa")

        };
        public async Task AddAsync (Group group) => await Task.FromResult (_group.Add (group));

        public async Task<IEnumerable<Group>> BrowseAsync (string name = "") {
            var groups = _group.AsEnumerable<Group> ();
            if (!String.IsNullOrWhiteSpace (name))
                groups = groups.Where (x => x.Name.ToLowerInvariant ().Contains (name));
            return await Task.FromResult (groups);
        }
        public async Task<Group> GetAsync (Guid id) => await Task.FromResult (_group.SingleOrDefault (x => x.Id.Equals (id)));
        public async Task<Group> GetAsync (string name) => await Task.FromResult (_group.SingleOrDefault (x => x.Name.Equals (name)));

        public async Task RemoveAsync (Group group) => await Task.FromResult (_group.Remove (group));
        public async Task UpdateAsync (Group group) {
            await Task.CompletedTask;
        }
    }
}