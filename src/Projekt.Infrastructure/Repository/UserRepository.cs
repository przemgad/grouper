using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Projekt.Core.Domain;
using Projekt.Core.Repositories;

namespace Projekt.Infrastructure.Repository {
    public class UserRepository : IUserRepository {
    private readonly static ISet<User> _users = new HashSet<User> {
    User.CreateUser (Guid.NewGuid (), "przemek", "mail@gmail.com", "hash"),
    User.CreateUser (Guid.NewGuid (), "Przemyslaw", "mallil@gmail.com", "hash"),
    User.CreateUser (Guid.NewGuid (), "1dojutr", "mail@gmail.com", "hash")
        };
        public async Task AddAsync (User user) => await Task.FromResult (_users.Add (user));

        public async Task<IEnumerable<User>> BrowseAsync (string name = "") {
            var users = _users.AsEnumerable<User> ();
            if (!String.IsNullOrWhiteSpace (name))
                users = users.Where (x => x.Name.ToLowerInvariant ().Contains (name));
            return await Task.FromResult (users);
        }

        public async Task<User> GetAsync (Guid id) => await Task.FromResult (_users.SingleOrDefault (x => x.Id.Equals (id)));

        public async Task<User> GetAsync (string email) => await Task.FromResult (_users.SingleOrDefault (x => x.Email.Equals (email)));

        public async Task RemoveAsync (User user) => await Task.FromResult (_users.Remove (user));

        public async Task UpdateAsync (User user) => await Task.CompletedTask;

    }
}