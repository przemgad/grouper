using System;
using System.Threading.Tasks;
using Projekt.Core.Domain;
using Projekt.Core.Repositories;
using Projekt.Infrastructure.DTO;

namespace Projekt.Infrastructure.Repository.ExtensionMethod
{
    public static class UserExtension
    {
        public static async Task<User> GetOrFailAsync(this IUserRepository _userRepostiory, Guid id)
        {
            var user = await _userRepostiory.GetAsync(id);
            if (user == null)
                throw new Exception($"User id:{id} does not exist.");
            else
                return user;
        }
        public static async Task<User> GetOrFailAsync(this IUserRepository _userRepostiory, string name)
        {
            var user = await _userRepostiory.GetAsync(name);
            if (user == null)
                throw new Exception($"User name:{name} does not exist.");
            else
                return user;
        }
    }
}