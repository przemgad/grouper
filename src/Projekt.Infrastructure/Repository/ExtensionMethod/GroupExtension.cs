using System;
using System.Threading.Tasks;
using Projekt.Core.Domain;
using Projekt.Core.Repositories;
using Projekt.Infrastructure.DTO;

namespace Projekt.Infrastructure.Repository.ExtensionMethod
{
    public static class GroupExtension
    {
        public static async Task<Group> GetOrFailAsync(this IGroupRepository _groupRepostiory, Guid id)
        {
            var group = await _groupRepostiory.GetAsync(id);
            if (group == null)
                throw new Exception($"Group id:{id} does not exist.");
            else
                return group;
        }
        public static async Task<Group> GetOrFailAsync(this IGroupRepository _groupRepostiory, string name)
        {
            var group = await _groupRepostiory.GetAsync(name);
            if (group == null)
                throw new Exception($"Group name:{name} does not exist.");
            else
                return group;
        }
    }
}