using AutoMapper;
using Projekt.Core.Domain;
using Projekt.Infrastructure.DTO;
using Projekt.Infrastructure.Services;

namespace Projekt.Infrastructure.MapperCFG {
    public class MapperCFG {

        public static IMapper Initialize () => new MapperConfiguration (cfg => {
                cfg.CreateMap<Group, GroupDTO> ();
                cfg.CreateMap<User, UserDTO> ();
                cfg.CreateMap<User, LocationDTO>();
            })
            .CreateMapper ();

    }
}