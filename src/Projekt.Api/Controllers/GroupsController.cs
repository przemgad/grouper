﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Projekt.Core.Domain;
using Projekt.Infrastructure.Command;
using Projekt.Infrastructure.Commands;
using Projekt.Infrastructure.DTO;
using Projekt.Infrastructure.Services;

namespace Projekt.Api.Controllers {
    [Route ("[controller]")]
    public class GroupsController : Controller {
        private readonly IGroupService _service;
        public GroupsController (IGroupService service) {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> Get (string name) {
            var groups = await _service.BrowseAsync (name);
            return Json (groups);
        }

        [HttpGet ("{id}")]
        public async Task<IActionResult> GetGroup (Guid id) {
            var groups = await _service.GetAsync (id);
            return Json (groups);
        }

        [HttpPut ("{id}")]
        public async Task<IActionResult> Put (Guid id, [FromBody] EditGroup command) {
                await _service.UpdateAsync (id, command.Name, command.Description);
                return NoContent ();
            }
        [HttpDelete ("{id}")]
        public async Task<IActionResult> Delete (Guid id) {
                await _service.DeleteAsync (id);
                return NoContent ();
            }
        [HttpPost ("{GroupId}/members")]
        public async Task<IActionResult> AddUser (Guid groupId, [FromBody] IdTaker user) {
                await _service.AddMemberAsync (groupId, user.Id);
                return NoContent ();
            }
        [HttpDelete ("{GroupId}/members")]
        public async Task<IActionResult> RemoveUser (Guid groupId, [FromBody] IdTaker user) {
            await _service.RemoveMemberAsync (groupId, user.Id);
            return NoContent ();
        }

    }
}