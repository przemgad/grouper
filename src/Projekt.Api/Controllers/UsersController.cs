﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Projekt.Core.Domain;
using Projekt.Infrastructure.Commands;
using Projekt.Infrastructure.DTO;
using Projekt.Infrastructure.Services;

namespace Projekt.Api.Controllers {
    [Route ("[controller]")]
    public class UsersController : Controller {
        private readonly IUserService _service;
        public UsersController (IUserService service) {
            _service = service;
        }

        [HttpGet]
        public async Task<IActionResult> Get (string name) {
            var groups = await _service.BrowseAsync (name);
            return Json (groups);
        }

        [HttpGet ("{id}")]
        public async Task<IActionResult> GetUser (Guid id) {
            var user = await _service.GetAsync (id);
            return Json (user);
        }

        [HttpDelete ("{id}")]
        public async Task<IActionResult> DeleteUser (Guid id) {
                await _service.DeleteAsync (id);
                return NoContent ();
            }
            [HttpPut ("{id}")]
        public async Task<IActionResult> Put (Guid id, [FromBody] EditUser command) {
            await _service.UpdateAsync (id, command.Name, command.Email, command.Password, command.Role);
            return NoContent ();
        }

        [HttpPost]
        public async Task<IActionResult> Post ([FromBody] CreateUser command) {
            command.Id = Guid.NewGuid ();
            command.Role = "user";
            await _service.CreateAsync (command.Id, command.Name, command.Email, command.Password);
            return Created ($"/users/{command.Id}", null);
        }
   
        [HttpGet ("{id}/location")]
        public async Task<IActionResult> GetLocation(Guid id){
            var location = await _service.GetLocationAsync(id);
            return Json(location);
        }
        [HttpPatch ("{id}/location")]
        public async Task<IActionResult> PatchLocation(Guid id, [FromBody]UpdateLocation command){
            await _service.UpdateLocationAsync(id, command.Latitude, command.Longitude);
            return NoContent();
        }

    }
}