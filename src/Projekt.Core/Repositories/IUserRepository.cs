using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projekt.Core.Domain;

namespace Projekt.Core.Repositories
{
    public interface IUserRepository
    {
         Task<IEnumerable<User>> BrowseAsync(string name = "");
         Task<User> GetAsync(Guid id);
         Task<User> GetAsync(string name);         
         Task UpdateAsync(User user);
         Task AddAsync(User user);
         Task RemoveAsync(User user);

    }
}