using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Projekt.Core.Domain;

namespace Projekt.Core.Repositories
{
    public interface IGroupRepository
    {
         Task<Group> GetAsync(Guid id);
         Task<Group> GetAsync(string name);         
         Task UpdateAsync(Group group);
         Task AddAsync(Group group);
         Task<IEnumerable<Group>> BrowseAsync(string name);
         Task RemoveAsync(Group group);
    }
}