using System;

namespace Projekt.Core.Domain
{
    public abstract class Entity
    {
        protected Guid id { get; set; }

        protected Entity()
        {
            id = Guid.NewGuid();
        }
    }
}