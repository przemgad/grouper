using System;
using System.Collections.Generic;

namespace Projekt.Core.Domain {
    public class Group : Entity {
        private ISet<User> _members = new HashSet<User> ();
        public User HeadUser { get; protected set; }
        public Guid Id { get; protected set; }
        public string Name { get; protected set; }
        public string Description { get; protected set; }
        public IEnumerable<User> Members => _members;
        protected Group () {

        }
        public Group (Guid id, string _name, string _description) {
            Id = id;
            SetName (_name);
            SetDescription (_description);

        }
        public void SetName (string name) {
            if (String.IsNullOrEmpty (name))
                throw new Exception ("Name cannot be empty or null");
            if (name.Length < 5)
                throw new Exception ("Name must be at least 5 characters ");
            this.Name = name;
        }
        public void SetDescription (string description) {
            if (String.IsNullOrEmpty (description))
                throw new Exception ("Name cannot be empty or null");
            else if (description.Length < 5)
                throw new Exception ("Name length must be greater than 5 char ");
            this.Description = description;
        }

        public static Group CreateGroup (Guid id, string _name, string _description) {
            if (String.IsNullOrEmpty (_name))
                throw new Exception ("Group must have a name.");
            else if (String.IsNullOrEmpty (_description))
                throw new Exception ("Group must have a description.");
            return new Group (id, _name, _description);
        }
        public void AddToMembers (User user) {
            if (_members.Contains (user))
                throw new Exception ($"User name:{user.Name} is already in group");
            _members.Add (user);
        }
        public void RemoveFromMembers (User user) {
            if (_members.Contains (user))
                _members.Remove(user);
            else 
                throw new Exception($"Group doen't contain {user.Name}.");
        }
        

    }
}