using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Projekt.Core.Domain {
    public class User : Entity {
        private ISet<Group> _groups = new HashSet<Group> ();
        public Guid Id { get; protected set; }
        public string Role { get; set; }
        public string Name { get; protected set; }
        public string Email { get; protected set; }
        public string Password { get; protected set; }
        public double? Latitude { get; protected set; }
        public double? Longitude { get; protected set; }
        public IEnumerable<Group> Groups => _groups;

        protected User () {

        }
        public User (Guid id, string name, string email, string password) {
            Id = id;
            setName(name);
            setEmail(email);
            Password = password;

        }
        public static User CreateUser (Guid id, string name, string email, string password) {
            return new User (id, name, email, password);
        }
        public void joinToGroup (Group group) {
            if (group == null)
                throw new Exception ("Group cannot be Null");
            else if (_groups.Contains (group))
                throw new Exception ("You are already a member of this group.");
            else _groups.Add (group);

        }
        public void setName (string name) {
            if (name == null)
                throw new Exception ("Name cannot be Null");
            if (name.Length < 5)
                throw new Exception ("Name must contain minimum 5 alphanumeric characters");
            this.Name = name;
        }
        public void setEmail (string email) {
            if (email == null)
                throw new Exception ("Email cannot be Null");
            string pattern = @"^[a-z][a-z|0-9|]*([_][a-z|0-9]+)*([.][a-z|0-9]+([_][a-z|0-9]+)*)?@[a-z][a-z|0-9|]*\.([a-z][a-z|0-9]*(\.[a-z][a-z|0-9]*)?)$";
            Match match = Regex.Match(email.Trim(), pattern, RegexOptions.IgnoreCase);
            if (!match.Success)
                throw new Exception ("Invalide pattern email.");
            this.Email = email;
        }
        public void setLocation (double latitude, double longitude) {
            this.Latitude = latitude;
            this.Longitude = longitude;
        }

    }

}